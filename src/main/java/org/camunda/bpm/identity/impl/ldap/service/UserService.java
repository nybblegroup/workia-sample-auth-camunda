package org.camunda.bpm.identity.impl.ldap.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.identity.impl.ldap.LdapUserEntity;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

public class UserService {

	//TODO Override to repository
    public User findById(String id) {
    	System.out.println("findById");
	  	LdapUserEntity user = new LdapUserEntity();
	  	user.setId("demo");
	  	user.setFirstName("Demo");
	  	user.setPassword("pass");
	  	user.setLastName("Camunda");
	  	user.setDn("demoDn");
		System.out.println(user);
        return (User) user;
    }

    public Collection<User> findAll() {
	  	LdapUserEntity user1 = new LdapUserEntity();
	  	System.out.println("findAll");
	  	user1.setId("demo");
	  	user1.setFirstName("Demo");
	  	user1.setPassword("pass");
	  	user1.setLastName("Camunda");
		user1.setDn("demoDn");
		System.out.println(user1);
	  
		List<User> users = new  ArrayList<User>();
		users.add(user1);
        return (Collection<User>) users;
    }

}

